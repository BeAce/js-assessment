module.exports = {
  alterContext: function(fn, obj) {
    return fn.apply(obj);
  },

  alterObjects: function(constructor, greeting) {
    constructor.prototype.greeting = greeting;
  },

  iterate: function(obj) {
    const ret = [];
    Object.keys(obj).forEach(item => {
      ret.push(`${item}: ${obj[item]}`);
    });
    return ret;
  },
};
