module.exports = {
  listFiles: function(data, dirName) {
    const listOfFiles = [];
    const dirs = [];

    function processDir(dir) {
      let file;
      const files = dir.files;

      dirs.push(dir.dir);

      for (let i = 0, len = files.length; i < len; i++) {
        file = files[i];
        if (typeof file === 'string') {
          if (!dirName || dirs.indexOf(dirName) > -1) {
            listOfFiles.push(files[i]);
          }
        } else {
          processDir(files[i]);
        }
      }

      dirs.pop();
    }

    processDir(data);

    return listOfFiles;
  },

  permute: function(arr) {
    // http://stackoverflow.com/a/11509565/54468
    const temp = [];
    const answer = [];

    function logResult() {
      answer.push(
        // make a copy of temp using .slice()
        // so we can continue to work with temp
        temp.slice()
      );
    }

    function doIt() {
      let item;

      for (let i = 0, len = arr.length; i < len; i++) {
        // remove the item at index i
        item = arr.splice(i, 1)[0];

        // add that item to the array we're building up
        temp.push(item);

        if (arr.length) {
          // if there's still anything left in the array,
          // recurse over what's left
          doIt();
        } else {
          // otherwise, log the result and move on
          logResult();
        }

        // restore the item we removed at index i
        // and remove it from our temp array
        arr.splice(i, 0, item);
        temp.pop();
      }

      return answer;
    }

    return doIt();
  },

  fibonacci: function(n) {
    return n <= 2 ? 1 : arguments.callee(n - 1) + arguments.callee(n - 2);
  },

  validParentheses: function(n) {
    const vParens = function(nInPar, nOutPar) {
      const result = [];
      let theRest = [];
      // terminal case
      if (nOutPar === 0) {
        return [''];
      }
      if (nInPar > 0) {
        // can output '('
        // recursion
        theRest = vParens(nInPar - 1, nOutPar);
        theRest.forEach(el => result.push('('.concat(el)));
      }
      if (nOutPar > nInPar) {
        // can output ')'
        // recursion
        theRest = vParens(nInPar, nOutPar - 1);
        theRest.forEach(el => result.push(')'.concat(el)));
      }
      return result;
    };
    return vParens(n, n);
  },
};
