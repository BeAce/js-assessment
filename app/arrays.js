module.exports = {
  indexOf: (arr, item) => arr.findIndex(i => i === item),

  sum: (arr) => arr.reduce((i, j) => i + j, 0),

  remove: (arr, item) => arr.filter(i => i !== item),

  removeWithoutCopy: (arr, item) => {
    for (let i = arr.length - 1; i >= 0; i-=1) {
      if (arr[i] === item) {
        arr.splice(i, 1);
      }
    }
    return arr;
  },

  append: (arr, item) => {
    arr.push(item);
    return arr;
  },

  truncate: (arr) => {
    arr.splice(arr.length - 1, 1);
    return arr;
  },

  prepend: (arr, item) => {
    arr.unshift(item);
    return arr;
  },

  curtail: (arr) => {
    arr.shift();
    return arr;
  },

  concat: (arr1, arr2) => {
    return [...arr1, ...arr2];
  },

  insert: (arr, item, index) => {
    arr.splice(index, 0, item);
    return arr;
  },

  count: (arr, item) => {
    let temp = 0;
    arr.forEach(i => {
      if (i === item) temp+=1;
    });
    return temp;
  },

  duplicates: (arr) => {
    const temp = [];
    arr.forEach((item, index) => {
      if (arr.indexOf(item) !== index && temp.indexOf(item) < 0) {
        temp.push(item);
      }
    });
    return temp;
  },

  square: (arr) => {
    return arr.map(item => item * item);
  },

  findAllOccurrences: (arr, target) => {
    const temp = [];
    arr.forEach((item, index) => {
      if (item === target) {
        temp.push(index);
      }
    });
    return temp;
  }
};
