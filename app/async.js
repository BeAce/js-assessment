module.exports = {
  async: function(value) {
    return Promise.resolve(value);
  },

  manipulateRemoteData: function(url) {
    return fetch(url).then(res => res.json()).then(data => {
      return data.people.map(item => item.name).sort();
    });
  }
};
