/* eslint-disalbe */
module.exports = {
  argsAsArray: function(fn, arr) {
    return fn(...arr);
  },

  speak: function(fn, obj) {
    // 1
    // Object.keys(obj).forEach(item => {
    //   this[item] = obj[item];
    // });
    // return fn.call(this);
    // 2
    return fn.call(obj);
  },

  functionFunction: function(str) {
    return function(str2) {
      return `${str}, ${str2}`;
    };
  },

  makeClosures: function(arr, fn) {
    return arr.map(item => {
      return function() {
        return fn(item);
      };
    });
  },

  partial: function(fn, str1, str2) {
    return function(str) {
      return fn(str1, str2, str);
    };
  },

  useArguments: function() {
    return [...arguments].reduce((i, j) => i + j, 0);
  },

  callIt: function(fn) {
    const args = [...arguments];
    fn.apply(null, args.slice(1));
  },

  partialUsingArguments: function(fn) {
    const args = [...arguments].slice(1);
    return function() {
      return fn.apply(null, args.concat([...arguments]));
    };
  },

  curryIt: function(fn) {
    function applyArgs(_fn, args) {
      return _fn.apply(null, args);
    }
    function getArgumentAccumulator(args, count) {
      return function(currentArgs) {
        args.push(currentArgs);
        const allArgs = args.length === count;
        if (allArgs) {
          return applyArgs(fn, args);
        }
        return getArgumentAccumulator(args, count);
      };
    }

    return getArgumentAccumulator([], fn.length);
  },
};
