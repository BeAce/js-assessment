module.exports = {
  reduceString: function(str, amount) {
    const newStr = [];
    let prevLetter;
    let curLetter;
    let curCount = 0;
    for (let i = 0; i < str.length; i++) {
      curLetter = str[i];
      // Looking at a currently repeated letter
      // or the first letter
      if (curLetter === prevLetter || !prevLetter) {
        if (curCount < amount) {
          newStr.push(curLetter);
        }
        curCount++;
      } else {
        // Looking at a new letter
        newStr.push(curLetter);
        curCount = 1;
      }
      prevLetter = curLetter;
    }
    return newStr.join('');
  },

  wordWrap: function(str, cols) {
    const newStr = [];
    const words = str.split(' ');
    let curLine = [];
    for (let i = 0; i < words.length; i++) {
      // first word in the line
      if (!curLine.length) {
        curLine.push(words[i]);
      } else {
        // What the current line length would be if the current word as added
        // including the space in between the words
        const curLineLetterCount = curLine.join('').length;
        const lineLengthWithWord = curLineLetterCount + words[i].length + 1;
        if (lineLengthWithWord <= cols) {
          curLine.push(' ');
          curLine.push(words[i]);
        } else {
          // current word needs to go on a new line
          curLine.push('\n');
          newStr.push(curLine.join(''));

          curLine = [];
          curLine.push(words[i]);
        }
      }
    }
    // handle the last word
    if (curLine.length) {
      newStr.push(curLine.join(''));
    }
    return newStr.join('');
  },

  reverseString: function(str) {
    return str
      .split('')
      .reverse()
      .join('');
  },
};
