# A test-driven JS assessment

[![pipeline status](https://git.beacelee.com/beace/js-assessment/badges/master/pipeline.svg)](https://git.beacelee.com/beace/js-assessment/commits/master)   [![coverage report](https://git.beacelee.com/beace/js-assessment/badges/master/coverage.svg)](https://git.beacelee.com/beace/js-assessment/commits/master)

This repo includes a set of tests that can be used to assess the skills of
a candidate for a JavaScript position, or to evaluate and improve one's own
skills.

## I want to work on the tests; what do I do?
To use the tests, you will need to install [Node](https://nodejs.org/). Note
that on Windows, there are some reports that you will need to restart
after installing Node - see #12.

You can clone or download this repo. Once you have done so, from the root
directory of the repo, run:

    npm install
    npm test

The command line runner is a work in progress; contributions welcome :)

### Data-driven tests

If your tests need data that can be fetched via XHR, stick a `.json` file in
the `data` directory; you can access it at `/data/<filename>.json`.